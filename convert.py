# -*- coding: utf-8 -*-

import os
import sys
import csv
import json
import argparse
import logging


class CSVToJsonConverter:
    """A class that combines as set of CSV files into one JSON file

    To avoid over-complicating this, I will accumulate the json file in memory
    and write the output once. This obviously may not scale very well
    with massive csv files.

    Args:
        csv_files (list): A list of strings representing the paths of
                          input CSV files
        output_file (str): Path to the output json file
        logger (obj): Python Logger object
        should_force (boolean): Whether or not to continue execution
                                inspite of errors (default=False)
        code (:obj:`int`, optional): Error code.

    """
    required_fields = ['id', 'first_name', 'last_name', 'email']

    class InvalidRecordException(Exception):
        pass

    class InvalidFileException(Exception):
        pass

    def __init__(self, csv_files, output_file, logger, should_force=False):
        self.duplicate_lookup = set()
        self.output_file = output_file
        self.out_data = []
        self.csv_files = csv_files
        self.should_force = should_force
        self.logger = logger
        self.is_processed = False

    def process(self):
        """Begins operating on CSV files

        This method is a bit large, but I prefer to not break it down to allow
        other devs to follow the logic in one place

        """

        if self.is_processed:
            raise Exception(
                "This object shouldn't be reused, create a new one instead")
        self.is_processed = True

        try:
            self.logger.info("Started processing")
            for csv_file in self.csv_files:

                self.logger.info("Processing file %s", csv_file)

                try:
                    with open(csv_file, newline='') as file_handle:
                        self.__process_csv(file_handle)

                # If we encounter an error while processing CSV file
                # we either exit or continue with the next file
                except (
                        FileNotFoundError,
                        CSVToJsonConverter.InvalidFileException,
                        CSVToJsonConverter.InvalidRecordException) as ex:

                        todo = (
                            "exitting" if self.should_force is False
                            else "moving on with the next file")
                        self.logger.exception(
                            "Failed to process CSV file (%s), %s",
                            ex, todo, exc_info=True)
                        if self.should_force:
                            # Continue with the next CSV
                            continue
                        else:
                            # Terminate
                            sys.exit(1)

            self.logger.info("Done reading CSV files")
            self.logger.info("Writing JSON file")

            # Warn the user if the file already exists,
            # no user intervention allowed
            if os.path.exists(self.output_file):
                self.logger.warning(
                    "Json file %s already exists, will overwrite",
                    self.output_file)

            # Write json file
            with open(self.output_file, 'w+') as file_handle:
                json.dump(self.out_data, file_handle)
            self.logger.info("Process complete, exiting")

        # Top level exception handling
        except KeyboardInterrupt:
            self.logger.info("Recieved termination signal, exitting")
            exit(1)

        except PermissionError:
            self.logger.exception(
                "Couldn't write to output file %s, Permission denied",
                self.output_file)
            exit(1)

        except SystemExit:
            exit(1)

        except:
            self.logger.exception("Unexpected error", exc_info=True)
            exit(1)

    def __process_csv(self, csv_file):
        """Reads, validates CSV files and appends valid records to internal list.

        To avoid over-complicating this, I will accumulate the json
        file in memory and write the output once.
        This obviously may not scale very well with massive csv files.
        """
        reader = csv.DictReader(csv_file)

        for idx, record in enumerate(reader):

            if idx == 0:
                # Validate file on first iteration
                # This is a CSV file, so we check only one row to make
                # sure all fields exist, we raise exception if a required
                # field is missing
                for f in CSVToJsonConverter.required_fields:
                    if f not in record:
                        raise CSVToJsonConverter.InvalidFileException(
                            "Invalid CSV file, missing field {}".format(f))

            if self.__validate_record(idx, record):
                self.out_data.append(record)

    def __validate_record(self, idx, record):
        """Validates CSV record.

        Currently, this checks for duplicate emails and empty
        required fields.
        """

        # Check for empty fields
        for f in CSVToJsonConverter.required_fields:
            if f not in record or len(record[f]) == 0:
                self.logger.warning(
                    "Record %s has empty %s field",
                    idx, f)
                return False

        # Check for duplicate emails
        if 'email' in record and record['email'] in self.duplicate_lookup:
            self.logger.warning("Duplicate email found (%s)", record['email'])
            return False
        self.duplicate_lookup.add(record['email'])

        # If you want to trigger validation errors that would stop script
        # execution (if --force is not enabled) you can raise
        # an InvalidRecordException here

        return True
