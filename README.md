# Python CSV to JSON converter
This is a python command line utility that takes in an arbitrary number of CSV files and converts them to a JSON file.

## Dependencies
This script is built for python 3, it has no dependencies outside python standard library.
## Installation instructions
1. ```git clone https://droushi@bitbucket.org/droushi/csv_to_json.git```
2. change directory to repo root.
3. run ```python csv_to_json.py --help```

## Usage instructions
A quick usage example goes like this

```csv_to_json.py csv_1.csv csv_2.csv csv_3.csv -o output.json```

the user specifies as many csv as they like and then provides the mandatory `-o` parameter which defines the output file.

### Arguments
usage: ```csv_to_json.py [-h] [-l log_file] [-f] [-v] -o JSON_File csv [csv ...]```


| Argument     | Description   																																													   |
| -------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| csv          | (required) One or more csv files to be processed. A valid CSV will contain id, first_name, last_name and email fields, other fields will be add to the output JSON file but will not be validated |
| -o JSON_file | (required) Path to the output json file. File will be overwritten if already exists                                                                                                               |
| -h           | (optional) Shows help message and exists 																																						   |
| -l logfile   | (optional) Specifies the log file to use, must be writable. (default=csv_to_json.log) 																											   |
| -f           | (optional) Forces the conversion to proceed in spite of errors 																																   |
| -v           | (optional) Outputs more information about conversion process to stdout 																														   |




