#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""A tool that combines multiple CSV files into a one JSON file

Example: csv_to_json.py csv_1.csv csv_2.csv -o output.json
"""

import os
import sys
import csv
import json
import argparse
import logging

from convert import CSVToJsonConverter

# First, command line parsing
parser = argparse.ArgumentParser(
    description="A tool that combines multiple CSV files into a one JSON file",
    epilog="Example: csv_to_json.py csv_1.csv csv_2.csv -o output.json"
)

parser.add_argument('csv', type=str, nargs='+', help="""One or more csv files
to be processed. A valid CSV will contain id, first_name,
last_name and email fields, other fields will be added
to the output JSON file but will not be validated""")

parser.add_argument('-l', '--logfile', default="csv_to_json.log",
                    metavar="log_file", type=str,
                    help="Specifies the log file to use, must be writable.")

parser.add_argument('-f', '--force', action='store_true', help="""Forces the
 conversion to proceed in spite of errors""")

parser.add_argument('-v', '--verbose', action='store_true', help="""Outputs more information
 about conversion process""")

required_grp = parser.add_argument_group('Required named arguments')
required_grp.add_argument('-o', metavar="JSON_File", type=str, required=True, help="""
An output JSON file, will be overwrittern if already exists""")

args = parser.parse_args()

# Then, we setup logging. We have tWo log handlers,
#  one for log files, the other for stdout
root = logging.getLogger(__name__)
root.setLevel(logging.DEBUG)

# Log file settings
logfile_fmt = logging.Formatter('[%(asctime)s] %(levelname)s : %(message)s')
logfile_handler = logging.FileHandler(args.logfile)
logfile_handler.setLevel(logging.INFO)
logfile_handler.setFormatter(logfile_fmt)
root.addHandler(logfile_handler)

# Stdout settings
stdout_fmt = logging.Formatter('[%(levelname)s]: %(message)s')
stdout_handler = logging.StreamHandler(sys.stdout)
# This is where verbose argument is used
stdout_handler.setLevel(logging.DEBUG if args.verbose else logging.INFO)
stdout_handler.setFormatter(stdout_fmt)
root.addHandler(stdout_handler)

# Send arguments to the converter
CSVToJsonConverter(args.csv, args.o, root, should_force=args.force).process()
